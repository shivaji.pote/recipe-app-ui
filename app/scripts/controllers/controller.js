'use strict';

/**
 * @ngdoc function
 * @name recipiAssignment1App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the recipiAssignment1App
 */
angular.module('recipiAssignmentApp')
  .controller('MainCtrl', ['$http', function ($http) {
    //externilize this property in prod code
    const SERVER_URL = "http://localhost:8080/rabo";

    var vm = this;
    var recipes;
    vm.selectedIngredients = [];
    vm.setting1 = {
      scrollableHeight: '200px',
      scrollable: true,
      enableSearch: true
    };

    function init() {
      vm.recipes = [];
      vm.logs = [];
      vm.viewRecipiApp();
    }

    vm.viewRecipiApp = function () {
      vm.showRecipiApp = true;
      vm.getRecipies();
      vm.showLogAnalyzerApp = false;
    };

    vm.viewLogAnalyzerApp = function () {
      vm.showRecipiApp = false;
      vm.showLogAnalyzerApp = true;
    };

    vm.getLogs = function () {
      vm.logs = [];
      $http.get(SERVER_URL + "/getLogOverview/" + vm.logType).then(function (response) {
        var map = response.data.resultMap;
        var keys = Object.keys(map);
        var values = Object.values(map);
        for (var key in keys) {
          var obj = {description: keys[key], count: values[key]};
          vm.logs.push(obj);
        }
      }).catch(function (error) {
        console.log('error occured while retrieving logs', error);
      });
    };

    vm.getRecipies = function () {
      if (angular.isUndefined(recipes) || angular.isArray(recipes) && !recipes.length) {
        recipes = [];
      }
      $http.get(SERVER_URL + "/getAllRecipes").then(function (response) {
        console.log(response);
        vm.recipes = response.data.recipies;
      }).catch(function (error) {
        console.log('error occured while retrieving recipes', error);
      });
    };

    vm.filterRecipies = function () {
      var selected = [];
      vm.selectedIngredients.forEach(function (ele) {
        //selected.push(ele.label);
        selected.push(vm.ingredients[ele.id].label);
      });
      vm.getAllRecipes(selected);
    };

    vm.getAllRecipes = function (ingredients) {
      if (angular.isUndefined(ingredients) || angular.isArray(ingredients) && !ingredients.length) {
        vm.getRecipies();
      } else {
        if (angular.isUndefined(recipes) || angular.isArray(recipes) && !recipes.length) {
          recipes = [];
        }
        $http.post(SERVER_URL + "/getRecipes", ingredients).then(function (response) {
          console.log(response);
          vm.recipes = response.data.recipies;
        }).catch(function (error) {
          console.log('error occured while retrieving recipes', error);
        });
      }
    };

    init();

    vm.ingredients = [{
      id: 0,
      label: 'onions'
    }, {
      id: 1,
      label: 'mushrooms'
    }, {
      id: 2,
      label: 'rosemary'
    }, {
      id: 3,
      label: 'olive oil'
    }, {
      id: 4,
      label: 'rice'
    }, {
      id: 5,
      label: 'ham'
    }, {
      id: 6,
      label: 'salt'
    }, {
      id: 7,
      label: 'chicken'
    }, {
      id: 8,
      label: 'butter'
    }, {
      id: 9,
      label: 'garlic'
    }, {
      id: 10,
      label: 'red onions'
    }, {
      id: 11,
      label: 'pastry'
    }];

  }]);
