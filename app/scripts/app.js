'use strict';

/**
 * @ngdoc overview
 * @name recipiAssignmentApp
 * @description
 * # recipiAssignment1App
 *
 * Main module of the application.
 */
angular.module('recipiAssignmentApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angularjs-dropdown-multiselect'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).constant('ConfigurationService', function (){
  return {
    APP_NAME: 'Assignment',
    URL_get_recipies: '/recipies',
    LABEL_CLIENT_ERROR: "Total Client Errors (4xx)",
    LABEL_SERVER_ERROR: "Total Server Errors (4xx)",
    LABEL_RESPONSE_OK: "Total Responses OK (200)",
  };
});

