'use strict';

var recipeData = {
    "recipies": [
      {
        "title":"Test Recipe 1",
        "href": "www.google.com",
        "thumbnail":"www.google.com",
        "ingredients" : ["onion","garlic"]
      },
      {
        "title":"Test Recipe 2",
        "href": "www.google.com",
        "thumbnail":"www.google.com",
        "ingredients" : ["onion","garlic"]
      },
      {
        "title":"Test Recipe 3",
        "href": "www.google.com",
        "thumbnail":"www.google.com",
        "ingredients" : ["onion","garlic"]
      }
  ]
};
var filteredIngredientsData = {
  "recipies": [
    {
      "title":"Test Recipe 1",
      "href": "www.google.com",
      "thumbnail":"www.google.com",
      "ingredients" : ["onion","garlic"]
    },
    {
      "title":"Test Recipe 3",
      "href": "www.google.com",
      "thumbnail":"www.google.com",
      "ingredients" : ["onion","garlic"]
    }
  ]
};

var logsData = {
  "resultMap" : {
    "this is test log description " : 5,
    "this is test log description 1" : 4,
    "this is test log description 2" : 4,
    "this is test log description 3" : 3,
    "this is test log description 5" : 2,
    "this is test log description 6" : 1,
  }
};

angular.module('recipiAssignmentAppMock', ['ngMockE2E']).run(function ($httpBackend) {

    $httpBackend.whenGET(/\.html/).passThrough();

    var SERVER_URL = "http://localhost:8080/rabo";

  //var statusOk = (typeof statusCode !== 'undefined') ? statusCode : 200;
    var contentTypeJson = {"content-type": "application/json"};

    $httpBackend.whenGET(SERVER_URL + '/getLogOverview/ERROR').respond(200, logsData, contentTypeJson);
    $httpBackend.whenGET(SERVER_URL + '/getAllRecipes').respond(200, recipeData, contentTypeJson);
    $httpBackend.whenPOST(SERVER_URL + '/getRecipes').respond(200, filteredIngredientsData, contentTypeJson);
});
