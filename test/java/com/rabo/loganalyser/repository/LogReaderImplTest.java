package com.rabo.loganalyser.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.rabo.config.TestConfig;
import com.rabo.loganalyser.model.LogRow;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
public class LogReaderImplTest extends TestConfig {

	protected final ObjectMapper mapper = new ObjectMapper();

	@InjectMocks
	private final LogReader logReader = new LogReaderImpl();

	@Mock
	Environment env;

	@Before
	public void init() {
		MockitoAnnotations.openMocks(this);
		ReflectionTestUtils.setField(logReader, "env", env);
		Mockito.when(env.getProperty(Mockito.eq("log.analyzer.data.location")))
				.thenReturn("logAnalyser/test-log-file.log");

	}

	@Test
	public void testGetLogs_ReturnAllLogsFromLogFile() throws JsonParseException, JsonMappingException, IOException {
		final List<LogRow> logs = logReader.getLogs("ERROR");
		assertNotNull(logs);
		assertEquals(8, logs.size());
		final List<LogRow> filteredLogs = logs.stream()
				.filter(log -> log.getDescription()
						.equalsIgnoreCase("[main] Application: Error with Spring profile(s) : [dev]"))
				.collect(Collectors.toList());
		assertEquals(5, filteredLogs.size());
	}

}
