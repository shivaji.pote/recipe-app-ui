package com.rabo.loganalyser.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.rabo.config.TestConfig;
import com.rabo.loganalyser.model.LogAnalyserRequest;
import com.rabo.loganalyser.model.LogAnalyserResponse;
import com.rabo.loganalyser.repository.LogReaderImpl;
import com.rabo.loganalyser.service.LogAnalyserService;

@SpringBootTest(classes = LogReaderImpl.class)
public class LogAnalyserControllerTest extends TestConfig {

	@InjectMocks
	private final LogAnalyserController controller = new LogAnalyserController();

	@Mock
	private LogAnalyserService logAnalyserService;

	private MockMvc mockMvc;

	@Before
	public void init() {
		MockitoAnnotations.openMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
	}

	@Test
	public void testLogAnalyzerControllers_RespondsStatusOK() throws Exception {
		Mockito.when(logAnalyserService.getLogOverview(Mockito.any(LogAnalyserRequest.class)))
				.thenReturn(mockedResponse());
		mockMvc.perform(get("/getLogOverview/ERROR")).andExpect(status().isOk());
		verify(logAnalyserService, times(1)).getLogOverview(Mockito.any(LogAnalyserRequest.class));
	}

	private LogAnalyserResponse mockedResponse() {
		final LogAnalyserResponse response = new LogAnalyserResponse();
		final Map<String, Long> resultMap = new LinkedHashMap<>();
		response.setResultMap(resultMap);
		return response;
	}

}
