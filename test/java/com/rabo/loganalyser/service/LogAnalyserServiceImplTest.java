package com.rabo.loganalyser.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rabo.loganalyser.model.LogAnalyserRequest;
import com.rabo.loganalyser.model.LogAnalyserResponse;
import com.rabo.loganalyser.model.LogRow;
import com.rabo.loganalyser.repository.LogReader;
import com.rabo.loganalyser.service.LogAnalyserService;
import com.rabo.loganalyser.service.LogAnalyserServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
public class LogAnalyserServiceImplTest {

	protected final ObjectMapper mapper = new ObjectMapper();

	@InjectMocks
	private final LogAnalyserService logAnalyserService = new LogAnalyserServiceImpl();

	@Mock
	private LogReader logReader;

	@Before
	public void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void test_returnLogOverview() throws IOException {
		final LogAnalyserRequest logAnalyserRequest = new LogAnalyserRequest();
		final String logType = "ERROR";
		logAnalyserRequest.setLogType(logType);
		when(logReader.getLogs(Mockito.anyString())).thenReturn(mockedLogs());
		final LogAnalyserResponse response = logAnalyserService.getLogOverview(logAnalyserRequest);
		assertNotNull(response);
		assertNotNull(response.getResultMap());
		assertFalse(response.getResultMap().isEmpty());
		assertEquals(4, response.getResultMap().size());
		final Set<Map.Entry<String, Long>> entrySet = response.getResultMap().entrySet();
		final Iterator<Entry<String, Long>> iterator = entrySet.iterator();
		final Entry<String, Long> first = iterator.next();
		assertEquals("[localhost-startStop-1] DatabaseConfiguration: Configuring Liquibase", first.getKey());
		assertEquals("6", first.getValue().toString());
		final Entry<String, Long> second = iterator.next();
		assertEquals("[localhost-startStop-1] DatabaseConfiguration: Configuring Liquibase error", second.getKey());
		assertEquals("2", second.getValue().toString());
	}

	private List<LogRow> mockedLogs() {
		final List<LogRow> rows = new ArrayList<>();
		final LogRow row1 = new LogRow();
		row1.setDescription("[localhost-startStop-1] DatabaseConfiguration: Configuring Liquibase");
		row1.setLogLevel("ERROR");
		rows.add(row1);
		final LogRow row2 = new LogRow();
		row2.setDescription("[localhost-startStop-1] DatabaseConfiguration: Configuring Liquibase");
		row2.setLogLevel("ERROR");
		rows.add(row2);
		final LogRow row3 = new LogRow();
		row3.setDescription("[localhost-startStop-1] DatabaseConfiguration: Configuring Liquibase warn");
		row3.setLogLevel("ERROR");
		rows.add(row3);
		final LogRow row4 = new LogRow();
		row4.setDescription("[localhost-startStop-1] DatabaseConfiguration: Configuring Liquibase error");
		row4.setLogLevel("ERROR");
		rows.add(row4);
		final LogRow row5 = new LogRow();
		row5.setDescription("[localhost-startStop-1] DatabaseConfiguration: Configuring Liquibase");
		row5.setLogLevel("ERROR");
		rows.add(row5);
		final LogRow row6 = new LogRow();
		row6.setDescription("[localhost-startStop-1] DatabaseConfiguration: Configuring Liquibase");
		row6.setLogLevel("ERROR");
		rows.add(row6);
		final LogRow row7 = new LogRow();
		row7.setDescription("[localhost-startStop-1] DatabaseConfiguration: Configuring Liquibase info");
		row7.setLogLevel("ERROR");
		rows.add(row7);
		final LogRow row8 = new LogRow();
		row8.setDescription("[localhost-startStop-1] DatabaseConfiguration: Configuring Liquibase error");
		row8.setLogLevel("ERROR");
		rows.add(row8);
		final LogRow row9 = new LogRow();
		row9.setDescription("[localhost-startStop-1] DatabaseConfiguration: Configuring Liquibase");
		row9.setLogLevel("ERROR");
		rows.add(row9);
		final LogRow row10 = new LogRow();
		row10.setDescription("[localhost-startStop-1] DatabaseConfiguration: Configuring Liquibase");
		row10.setLogLevel("ERROR");
		rows.add(row10);

		return rows;
	}

}
