/**
 *
 */
package com.rabo.config;

import org.springframework.test.context.TestPropertySource;

/**
 * @author Shivaji Pote
 *
 */

@TestPropertySource("classpath:application-test.properties")
public abstract class TestConfig {

}
