package com.rabo.recipe.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.rabo.recipe.model.RecipeOverview;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(classes = RecipeRepositoryImpl.class)
public class RecipeRepositoryImplTest {

	protected final ObjectMapper mapper = new ObjectMapper();

	@InjectMocks
	private final RecipeRepository recipeRepo = new RecipeRepositoryImpl();

	@Mock
	Environment env;

	@Before
	public void init() {
		MockitoAnnotations.openMocks(this);
		ReflectionTestUtils.setField(recipeRepo, "env", env);
		Mockito.when(env.getProperty(Mockito.eq("recipe.data.location"))).thenReturn("recipe/recipeTest.json");

	}

	@Test
	public void testGetRecipeOverview_ReturnAllRecipyListFromJSONFile()
			throws JsonParseException, JsonMappingException, IOException {
		final RecipeOverview overview = recipeRepo.getRecipeOverview();
		assertNotNull(overview);
		assertNotNull(overview.getRecipies());
		assertEquals(4, overview.getRecipies().size());
	}

}
