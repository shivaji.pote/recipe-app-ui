package com.rabo.recipe.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.rabo.config.TestConfig;
import com.rabo.loganalyser.repository.LogReaderImpl;
import com.rabo.recipe.model.Recipe;
import com.rabo.recipe.model.RecipeOverview;
import com.rabo.recipe.service.RecipeService;

@SpringBootTest(classes = LogReaderImpl.class)
public class RecipeControllerTest extends TestConfig {

	@InjectMocks
	private final RecipeController controller = new RecipeController();

	@Mock
	private RecipeService recipeService;

	private MockMvc mockMvc;

	@Before
	public void init() {
		MockitoAnnotations.openMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
	}

	@Test
	public void testRecipeControllerForAllRecipes_RespondsStatusOK() throws Exception {
		Mockito.when(recipeService.getResponseRecipeOverview()).thenReturn(mockedResponse());
		mockMvc.perform(get("/getAllRecipes")).andExpect(status().isOk());
		verify(recipeService, times(1)).getResponseRecipeOverview();
	}

	@Test
	public void testRecipeControllerForSelectedIngredients_ReturnsStatusOK() throws Exception {
		when(recipeService.getResponseIngredientRecipes(Mockito.anyList())).thenReturn(mockedResponse());
		mockMvc.perform(post("/getRecipes").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("[\"onions\", \"mushrooms\"]")).andExpect(status().isOk());
		verify(recipeService, times(1)).getResponseIngredientRecipes(Mockito.anyList());
	}

	private RecipeOverview mockedResponse() {
		final RecipeOverview response = new RecipeOverview();
		final List<Recipe> recipes = new ArrayList<>();
		response.setRecipies(recipes);
		return response;
	}

}
