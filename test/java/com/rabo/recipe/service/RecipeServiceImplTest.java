package com.rabo.recipe.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.rabo.recipe.model.Recipe;
import com.rabo.recipe.model.RecipeOverview;
import com.rabo.recipe.repository.RecipeRepository;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

//@SpringBootTest//(classes = LogReaderImpl.class)
//@TestPropertySource(locations = "classpath:application-test.properties")
@ExtendWith(MockitoExtension.class)
public class RecipeServiceImplTest {

	protected final ObjectMapper mapper = new ObjectMapper();

	@InjectMocks
	private final RecipeService recipeService = new RecipeServiceImpl();

	@Mock
	private RecipeRepository recipeRepository;

	@Before
	public void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void test_returnsData() throws JsonParseException, JsonMappingException, IOException {
		when(recipeRepository.getRecipeOverview()).thenReturn(mockRecipeOverview());
		final List<Recipe> recipeOverviewList = recipeRepository.getRecipeOverview().getRecipies();
		assertNotNull(recipeOverviewList);
		assertEquals(4, recipeOverviewList.size());
	}

	@Test
	public void test_returnsResponseIngredientRecipes() throws JsonParseException, JsonMappingException, IOException {
		final List<String> ingredients = new ArrayList<>();
		ingredients.add("onions");
		ingredients.add("meat");
		when(recipeRepository.getRecipeOverview()).thenReturn(mockRecipeOverview());
		final RecipeOverview response = recipeService.getResponseIngredientRecipes(ingredients);
		assertNotNull(response);
		assertNotNull(response.getRecipies());
		assertEquals(2, response.getRecipies().size());
	}

	@Test
	public void test_returnsNoResponseIngredientRecipes() throws JsonParseException, JsonMappingException, IOException {
		final List<String> ingredients = new ArrayList<>();
		ingredients.add("garlic");
		ingredients.add("pork");
		when(recipeRepository.getRecipeOverview()).thenReturn(mockRecipeOverview());
		final RecipeOverview response = recipeService.getResponseIngredientRecipes(ingredients);
		assertNotNull(response);
		assertNotNull(response.getRecipies());
		assertEquals(0, response.getRecipies().size());
	}

	private RecipeOverview mockRecipeOverview() throws JsonParseException, JsonMappingException, IOException {
		final RecipeOverview mockedRecipeOverview = getRecipeOverview();
		return mockedRecipeOverview;
	}

	protected InputStream readFile(final String fileName) {
		return Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
	}

	protected RecipeOverview getRecipeOverview() throws JsonParseException, JsonMappingException, IOException {
		final List<Recipe> recipeList = mapper.readValue(readFile("recipe/recipeTest.json"),
				new TypeReference<List<Recipe>>() {
				});
		final RecipeOverview recipeOverview = new RecipeOverview();
		recipeOverview.setRecipies(recipeList);
		return recipeOverview;
	}

}
